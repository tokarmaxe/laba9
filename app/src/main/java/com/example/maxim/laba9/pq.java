package com.example.maxim.laba9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

public class pq extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pq);
    }

    public void onClick(View view){
        EditText pET = (EditText)findViewById(R.id.p);
        String p = pET.getText().toString();
        EditText qET = (EditText)findViewById(R.id.q);
        String q = qET.getText().toString();

        Intent intent = new Intent();
        intent.putExtra("p",p);
        intent.putExtra("q",q);
        setResult(1, intent);
        finish();
    }
}
