package com.example.maxim.laba9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    public int p, q;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p = 3;
        q = 7;
        int n = p * q;
        int fn = (p - 1) * (q - 1);
        int e = 5;

        int d = findTheReverse(e,fn);
        int slovo = 19;
        double text = Math.pow(slovo,e)%21;

        double textDec = Math.pow(text,e)%21;

    }

    public void onClick(View view){
        switch (view.getId()){
            case R.id.pq:
                enterPQ();
                break;
        }
    }

    public int findTheReverse(int num, int fn) {
        int reverse = 0;
        for (int i = 1; i < 31; i++) {
            if ((num * i) % fn == 1) {
                reverse = i;
                break;
            }
        }
        return reverse;

    }

    public void enterPQ(){
        Intent intent = new Intent(this, pq.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("MyLogs", "requestCode=" +requestCode+", resultCode="+resultCode);
        if(resultCode==1){
            switch(requestCode){
                case 1:
                    TextView tvP = (TextView)findViewById(R.id.p);
                    TextView tvQ = (TextView)findViewById(R.id.q);
                    p = Integer.valueOf(data.getStringExtra("p"));
                    q = Integer.valueOf(data.getStringExtra("q"));
                    tvP.setText((tvP.getText()+String.valueOf(p)));
                    tvQ.setText((tvQ.getText()+String.valueOf(q)));
                    break;
                case 2:

           }
        }
    }
}
